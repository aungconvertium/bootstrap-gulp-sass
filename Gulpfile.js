var gulp = require('gulp'),
    del = require('del'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    usemin = require('gulp-usemin'),
    cssmin = require('gulp-cssmin'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    livereload = require('gulp-livereload');

var pkg = require('./package.json');

var bases = {
  root: './',
  src: 'app/',
  dist: 'dist/',
  distAssets: 'dist/assets/'
};

var paths = {
  html: ['*.html'],
  jss:  ['../bower_components/jquery/dist/jquery.js',
         '../bower_components/bootstrap/dist/js/bootstrap.js'],
  js:   ['assets/js/*.js'],
  css:  ['../bower_components/bootstrap/dist/css/bootstrap.css',
         'assets/css/*.css'],
  img:  ['assets/img/*.jpg',
         'assets/img/*.png',
         'assets/img/*.gif'],
  font: ['../bower_components/bootstrap/dist/fonts/*.*',
         'assets/fonts/*.*']
};

// Clean 'dist' folder
gulp.task('clean', function() {
  return del(['dist/**/*']);
});

// Copy files to 'dist'
gulp.task('copy', ['clean'], function() {
  // Copy html
  gulp.src(paths.html, {cwd: bases.src})
  .pipe(gulp.dest(bases.dist));

  // Copy images
  gulp.src(paths.img, {cwd: bases.src})
  .pipe(gulp.dest(bases.distAssets + 'img'));

  // Copy fonts
  gulp.src(paths.font, {cwd: bases.src})
  .pipe(gulp.dest(bases.distAssets + 'fonts'));

  // Copy Js
  gulp.src(paths.js, {cwd: bases.src})
  .pipe(rename(pkg.name + '-app.js'))
  .pipe(gulp.dest(bases.distAssets + 'js'));
});

// Concat css
gulp.task('concat-css', ['clean'], function() {
  return gulp.src(paths.css, {cwd: bases.src})
    .pipe(concat(pkg.name + '-style.css'))
    .pipe(gulp.dest(bases.distAssets + 'css'));
});

// Concat js
gulp.task('concat-js', ['clean'], function() {
  return gulp.src(paths.jss, {cwd: bases.src})
    .pipe(concat(pkg.name + '-components.js'))
    .pipe(gulp.dest(bases.distAssets + 'js'));
});

// Minify css
gulp.task('cssmin', ['clean'], function () {
  return gulp.src(paths.css, {cwd: bases.src})
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(concat(pkg.name + '-style.min.css'))
    .pipe(gulp.dest(bases.distAssets + 'css'));
});

// Minify js:components
gulp.task('uglify-components', ['clean'], function() {
  return gulp.src(paths.jss, {cwd: bases.src})
    .pipe(uglify())
    .pipe(concat(pkg.name + '-components.min.js'))
    .pipe(gulp.dest(bases.distAssets + 'js'));
});

// Minify js:app
gulp.task('uglify-app', ['clean'], function() {
  return gulp.src(paths.jss, {cwd: bases.src})
    .pipe(uglify())
    .pipe(concat(pkg.name + '-app.min.js'))
    .pipe(gulp.dest(bases.distAssets + 'js'));
});

// Minify js combination
gulp.task('uglify', ['uglify-components', 'uglify-app']);

// sass
gulp.task('sass', function() {
  return gulp.src('app/sass/main.sass')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('style.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/assets/css'));
});

// Usemin
gulp.task('usemin', ['clean'], function() {
  return gulp.src(paths.html, {cwd: bases.src})
    .pipe(usemin({
        js: [uglify]
      }))
    .pipe(gulp.dest(bases.dist));
});

// Jshint 'gulp lint'
gulp.task('lint', function() {
  return gulp.src(['app/assets/js/*.js', 'gulpfile.js', 'server.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Watch 'gulp watch'
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('app/sass/components/*.sass', ['sass']);
});

// Default task 'gulp'
gulp.task('default', ['clean', 'sass', 'concat-css', 'concat-js', 'cssmin', 'uglify', 'copy', 'usemin']);
